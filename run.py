from flask import Flask
from flask_restful import Api, Resource
from pathlib import Path
import json
import mimetypes
import os

ROOT = 'D:\\Users\\Aki'

app = Flask( __name__, static_folder=ROOT, static_url_path='/' )
api = Api(app)

class HelloWorld( Resource ):
    def get( self ):
        return { 'messsage': 'Hello World' }
api.add_resource( HelloWorld, '/' )


class Files( Resource ):
    def get ( self ):
        return self.searchFiles()
    
    def searchFiles( self ):
        self.dir = ROOT
        # Pathオブジェクトを生成
        pObj = Path( self.dir )
        itemlist = []
        # for file in list(pObj.glob("*")):
        for file in list(pObj.glob("**/*")):
            item = {'name': file.name}
            if os.path.isdir(file):
                item['type'] = 'dir'
            else:
                item['type'] = 'file'
            item['mime'] = mimetypes.guess_type(file.name)[0]
            item['path'] = (str(file)).replace(ROOT, '')
            itemlist.append(item)

        filesData = dict()
        filesData['count'] = len(itemlist)
        filesData['items'] = itemlist

        print(json.dumps(filesData, ensure_ascii=False))

        return json.dumps(filesData)

api.add_resource( Files, '/files')

#{count: 3
# items: [
#  {
    # name:
    # size:
    # type:
#  }
# ]
#}

# @app.route('/')
# def hello_world():
#     return 'Hello, World!'

@app.route("/<name>")
def hello_name(name):
    return "Hello, {}".format(name)

if __name__ == '__main__':
    print (app.url_map)
    app.run(debug=True, threaded=True)
    # app.run(debug=True, host='0.0.0.0', port=8888, threaded=True)  # デバッグモード、localhost:8888 で スレッドオンで実行
    # app.run(host='0.0.0.0', port=8888, threaded=True)  # デバッグモード、localhost:8888 で スレッドオンで実行